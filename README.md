# Kingdom

> Thy Kingdom hath come

DIY platform as a service with baked in environment-aware continuous delivery.

We take care of:

 - Configuration management (think puppet, chef, ansible)
 - Setting up build server (think jenkins, travis)
 - Deployment pipeline (think jenkins jobs, go by ThoughtWorks)
 - Deployment scripts (think capistrano, vmc, rsync)
 - Local vagrant environment
 - Reporting your build status, test coverage etc

## Usage

Create a `kingdom.yml` in the root of your git repo:

``` yml
---
environments:
  mainline:
    branch: develop
    continuous: yes
  stable:
    branch: master
    continuous: yes
  staging:
    branch: staging
    continuous: yes
  production:
    branch: production

files:
  - lib/
  - static/
  - package.json

test_files:
  - spec/
  
public_dir: static/

services:
  node: 0.10.25

scripts:
  pre:
    - mv static/maintenance-disabled.html static/maintenance.html
  stop:
    - npm stop
  init:
    - npm install
    - npm run-script build
  start:
    - npm start
  post:
    - mv static/maintenance.html static/maintenance-disabled.html
  test:
    - npm test
```

Then to build, run the following:

```
kingdom build
```

1. Your mainline will be deployed to a vagrant box provided by kingdom
2. If defined, your test command will be run against the test deploy
3. Each environment with `continuous: yes` will have the latest mainline changes
   pushed into it's branch and be deployed to the SSH connection defined if your
   tests pass and previous env deploys ran successfully.

To deploy a non-continuous environment use the `deploy` command:

```
kingdom deploy production
```

1. If your changes have not been built or are not passing the test script then
   a new build and test will be scheduled.
1. Once your latest changes are passing the changes will be pushed into the
   deploying environment provided they are in each environment defined before it.
   If they aren't in a previous environment they will be deployed there first.
2. The changes will then be deployed to your environments SSH connection.

To check the status of your last build and environments run:

```
kingdom status
```

This command will report the last build status and also which changes are
deployed to each environment.

## Deployment process

Once an environment has been cleared for deployment the following steps occur:

1. An SSH connection is made to the environment or to the kingdom provided
   vagrant box if no SSH defined.
2. If defined, the `pre_deploy` scripts are run.
3. If defined, the `stop` scripts are run.
4. The `files` are sent to the `ssh` connection or the vagrant box.
5. If defined, the `start` scripts are run.
6. If defined, the `post_deploy` scripts are run.

If the deploying environment is the mainline then, if defined, the `test` files
will be deployed after step 3.

Also if defined, the `test` scripts are run after step 6.

## Reasoning

Designed as a beginners or early-stage approach to continous delivery kingdom
aims to provide basic build/testing, config management and deployment to
multiple environments.

Everything happens via the client to reduce setup and time. A clone of your
git repo is made locally to the `.kingdom/` directory. This is so that we can
checkout the latest changes and have a working directory to deploy from that
does not affect your repo. Since we deploy from your local repo you do not have
to setup new SSH keys for bitbucket/github and cloning is fast.

SSHing to your servers also happens from your machine so you need only add your
SSH keys to your env using `ssh-add` command, or provide your password when
deploying. This again enables kingdom to no worry about auth issues.

## The future

We will provide an AWS AMI for you to use. It will be an Ubuntu LTS that knows
how to install the services you define in your `kingdom.yml`. Until then your
services will already need to be installed on any machine you SSH into.

We aim to provide online tools for checking the status of your builds. This will
also allow teams to coordinate deploys and share build status state (this is
currently local to each repo).
