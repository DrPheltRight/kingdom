fs = require 'fs'
gulp = require 'gulp'
{load_config, create_tasks, tasks} = require '../lib/tasks'
exec = require('child_process').exec

example_dir = fs.realpathSync("#{__dirname}/../example")
build_dir = "#{example_dir}/.kingdom"
config_file = "#{example_dir}/kingdom.yml"

describe 'Kingdom.Tasks', ->
  describe 'create_tasks', ->
    beforeEach ->
      gulp.tasks = {}
      create_tasks(gulp, config_file: config_file)

    it 'should add build task', ->
      expect(gulp.tasks.build).to.exist

    it 'should add deploy tasks for each env', ->
      for env in ['mainline', 'staging', 'production']
        expect(gulp.tasks["deploy:#{env}"]).to.exist

    it 'should add status task', ->
      expect(gulp.tasks.status).to.exist

  describe 'build task', ->
    config = load_config(config_file)

    rm_build_dir = (done) -> exec("rm -rf #{build_dir}", -> done())
    beforeEach(rm_build_dir)
    # after(rm_build_dir)

    it 'should create .kingdom/ directory', (done) ->
      tasks.build config, ->
        expect(fs.existsSync(build_dir)).to.be.true
        done()

    it 'should clone .git/ into ./kingdom/working_dir', (done) ->
      tasks.build config, ->
        expect(fs.existsSync("#{build_dir}/working_dir")).to.be.true
        done()

    it 'should update existing ./kingdom/working_dir', (done) ->
      tasks.build config, ->
        tasks.build config, ->
          done()

