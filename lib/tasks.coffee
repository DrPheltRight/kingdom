fs = require 'fs'
path = require 'path'
yaml = require 'js-yaml'
git = require 'nodegit'
mori = require 'mori'
rm_rf = require 'rimraf'
{exec} = require 'child_process'

load_config = (config_file) ->
  config = yaml.safeLoad(fs.readFileSync(config_file, 'utf8'))
  config.root_dir or= "#{path.dirname(config_file)}"
  config.build_dir or= "#{config.root_dir}/.kingdom"
  config.working_dir = "#{config.build_dir}/working_dir"
  config

clone = (config, done) ->
  {root_dir, working_dir} = config

  git.Repo.clone root_dir, working_dir, null, (err, repo) ->
    throw err if err
    done(repo)

update_clone = (config, done) ->
  exec "cd #{config.working_dir} && git reset --hard", -> # use libgit2
    git.Repo.open config.working_dir, (err, clone) ->
      throw err if err
      done(clone)

clone_or_update = (config, done) ->
  {root_dir, build_dir, working_dir} = config

  throw "No .git/ found in #{root_dir}" unless fs.existsSync("#{root_dir}/.git")
  fs.mkdirSync(build_dir) unless fs.existsSync(build_dir)

  if fs.existsSync(working_dir)
    update_clone(config, done)
  else
    clone(config, done)

checkout_env = (clone, env_config, done) ->
  clone.getBranch env_config.branch, (err, branch) ->
    throw err if err
    done()

remove_unwanted_files = (config, done) ->
  keep_dir = "#{config.build_dir}/keep"
  rm_rf.sync(keep_dir) if fs.existsSync(keep_dir)
  fs.mkdirSync(keep_dir)

  for file in config.files.concat(config.test_files or [])
    fs.renameSync("#{config.working_dir}/#{file}", "#{keep_dir}/#{file}")

  fs.readdir config.working_dir, (err, files) ->
    throw err if err

    for file in files when file isnt '.git'
      rm_rf.sync("#{config.working_dir}/#{file}")

    for file in config.files.concat(config.test_files or [])
      fs.renameSync("#{keep_dir}/#{file}", "#{config.working_dir}/#{file}")

    rm_rf.sync(keep_dir)
    done()

remove_unwanted_test_files = (config, done) ->
  for file in config.test_files
    rm_rf.sync("#{config.working_dir}/#{file}")
  done()

run_tests = (config, done) ->
  done()

deploy = (clone, env_config, done) ->
  checkout_env(clone, env_config)
  send_files(clone, env_config.ssh)

build_task = (config, done) ->
  clone_or_update config, (clone) ->
    checkout_env clone, config.environments.mainline, ->
      remove_unwanted_files config, ->
        run_tests config, ->
          remove_unwanted_test_files(config, done)

create_tasks = (gulp, {config_file} = {}) ->
  config = load_config(config_file)

  gulp.task('build', (done) -> build(config, done))

  gulp.task 'status', (done) ->
    latest_build config, (build) ->
      done()

  for env, env_config of config.environments
    gulp.task "deploy:#{env}", ['build'], (done) ->
      with_clone config, (clone) ->
        ensure_services(config.services, [env_config.ssh])
        deploy(clone, env_config, done)

module.exports =
  load_config: load_config
  create_tasks: create_tasks
  tasks:
    build: build_task
